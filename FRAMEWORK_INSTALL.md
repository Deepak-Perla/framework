# Installation on Linux

## Script Installation (only for linux system)    

1. Create a folder named **CDLI**.   

2. Download the ***setup.sh*** file from [here](https://gitlab.com/cdli/framework/-/raw/phoenix/develop/setup.sh?inline=false) and copy the file inside **CDLI** folder.   

3. Open the terminal inside **CDLI** and run following commands   

    Make ***setup.sh*** executable by running the following command   
    ```
    chmod +x setup.sh    
    ```   

    then run the following command to start script   
    
    ```
    ./setup.sh   
    ```   

4. Installation is a 3 step process. They are  :

    1. **Stage-1 Setup**


        1.1   Checks if **virtualenv** is present or not. If not then installs **virtualenv**.


        1.2   Checks if **git** is present or not. If not then installs **git**.


        1.3   Checks if **docker** is present or not. If not then displays message to install docker from [here](https://docs.docker.com/v17.12/install/).


        1.4   Create virtualenv **cdli_env**.


        Before executing script for **Stage-2 Setup**, it in necessary to run script inside virtual environment.


    2. **Stage-2 Setup**


        2.1   Detect if script is executed inside virtualenv '**cdli_env**'.


        2.2   Clone **cdli/framework** repository.


        2.3   Set up permissions for cloned repository.


        2.4   Clone submodules.


        2.5   Checks if **docker-compose** is present or not. If not then installs **docker-compose**.


        2.6   Ask User to create Gitlab Access Tokens (for reading registry images).


        Visit https://gitlab.com/profile/personal_access_tokens and create a new access token with the '**read_repository**' & '**read_registry**' scope.


        2.7   Prompt user to login with any one of link to fetch images.


        User will be asked to fill in their login credentials. They can use gitlab username and password.


        After successful login with either Link 1 or Link 2, Press '**3**' and enter to continue with setup.


        Before executing script for **Stage-3 Setup**, open new terminal and run following commands


         ```
            source cdli_env/bin/activate
            
            ./framework/dev/cdlidev.py up
        ```   

        Once the **cdli_dev.py** is running and fetches all the required docker image (when running for first time it may take   up to 10 mins or more), then execute script for **Stage-3 Setup** inside virtual environment.
   
    3. **Stage-3 Setup**   


        3.1   Detect if script is executed inside virtualenv '**cdli_env**'.


        3.2   Check if **cdlidev.py** is running.


        3.3   Check if 'mariadb' container is running.


        3.4   Check if 'pma' container is running.


        (Optional, pma : PhpMyAdmin) To visually verify if database is uploaded or not.


        3.5   Extract Database.

        Prompt user if they have Gitlab access token (created in **Stage-2 Setup**).


        3.5.1 Method 1 : (If user don't have access token)   
        Download the **cdli_db.gz** file and supply the full path of **cdli_db.gz** when prompted.


        3.5.2 Method 2 : (If they have access token)   
        Copy paste the access token when prompted and database will be downloaded from repository.


        Make sure you have **developer access to cdli repository** then only you will be able to fetch database.   


        3.6   Copy the required files to container (mariadb).


        3.7   Execute mysql commands inside container(mariadb) shell.


        3.8   Clean up cache created during setup.


        3.9 Database have been uploaded successfully. You can verify by visiting http://127.0.0.1:2355.   
        **Stage-3 Setup** can be used to upload updated database.
        
If any error occurs at any stage, try running that stage setup again. If error still persists then report [here](https://gitlab.com/cdli/framework/-/issues/219).

## Manual Installation   

The installation process has been according to Ubuntu 16.04   

1. On Ubuntu, confirm that the prerequisites are installed:   

    1.1. The system should have `python3` installed in it.</p>   

    1.2. Install `virtualenv` and `git`</p>   
        
    ```
    sudo apt install virtualenv git
    ```   

    1.3. Install docker by following the instructions on https://docs.docker.com/v17.12/install/.   

    Also note that on Linux, docker commands usually need to be prefixed with `sudo`.
    
    So, if you see any message saying _permission denied_ or something like that, then try running that using `sudo`.
    
    To avoid needing `sudo` for docker commands, add your username to the docker group:</p>   

    ```
    sudo usermod -aG docker $USER
    ```   

2. Create a virtual environment in python to install various dependency packages

    ```
    virtualenv cdli_env --python=python3
    ```

    We are naming the virtual environment as `cdli_env`.   

3. Shift to the virtual environment created

    ```
    source cdli_env/bin/activate
    ```

    After this, you would be able to see `(cdli_env)` in the beginning of your terminal prompt.

    This shows that the virtual environment has been activated.   

4. Clone the framework repository

    ```
    git clone https://gitlab.com/cdli/framework --depth 1
    ```

5. And run the following command

    ```
    setfacl -R -m default:user:2354:rwX,user:2354:rwX framework
    ```

    This command which sets up extended ACL permissions on the folder without requiring any changes to its owner, group, or mode.

6. Now if you want to clone the submodules also:-
 
    6.1 Shift to the framework directory
    ```        
        cd framework
    ```         

    6.2 Initialize your local configuration file

    ```
        git submodule init
    ```
    6.3 Fetch all the data from that submodules

    ```
        git submodule update
    ```


7. Install docker-compose in the python virtual-env

    ```
    pip install docker-compose
    ```

    This will install `docker-compose` package and various other packages on which this package depends on.

8. Login to docker image of `orchestrator/manifests`

    ```
    docker login https://registry.gitlab.com/v2/cdli/framework/orchestrator/manifests/0.0.3
    ```

    Now you will be asked to fill in your login credentials.

    **OR**

```
docker login https://registry.gitlab.com/
```

    You can use your gitlab username and password here, or visit https://gitlab.com/profile/personal_access_tokens and create a new access token with the `read_registry` scope and use that for the password instead.

9. Run the framework by the following command

    ```
    ./dev/cdlidev.py up
    ```

10. If it runs fine, you will be having the following services running:

    10.1 Nginx, available to the host at http://127.0.0.1:2354

    10.2 phpMyAdmin, available to the host at http://127.0.0.1:2355

    10.3 MariaDB, available internally to containers at tcp://mariadb:3306

    10.4 Redis, available internally to containers at tcp://redis:6379

  

11. You can also, have a look at the docker containers by running

    ```
    docker ps
    ```

    #### Docker Container Access Commands

    11.1 Docker compose names containers following the scheme `cdlidev_[app_][name]_1`.

    11.2 For example, MariaDB will be named `cdlidev_mariadb_1` and the search app named `cdlidev_app_search_1`.

    11.3 To start a live shell in a container: `docker exec -ti [container_name] sh`

    11.4 To copy a file to a container's `/tmp` folder: `docker cp [local_path] [container_name]:[container_path]`

12. Just to confirm that everything is fine till here

    12.1 Open the link, http://127.0.0.1:2354 and you will see some login page. If you are able to see it, then you are going on fine.

    12.2 Also, open http://127.0.0.1:2355, and you will see the _phpMyAdmin_ login page. The credentials are the default one i.e.

    12.2.1  _username_: 'root'

    12.2.2  _password_: ''

13. Get the latest `.sql` file of the database(you can get the database file from 'database_schema' slack channel of CDLI. For more information, click here - https://gitlab.com/cdli/framework#contribution-development). If you have a file with `.sql.gz` extentension unzip it with running `gzip -d file.sql.gz`. Now we assume the filename to be `cdli_db.sql`.

14. Move the database to the docker container by running the following command:-
	```
	docker cp cdli_db.sql cdlidev_mariadb_1:/tmp/cdli_db.sql
	```

15. Now open the MariaDB instance to upload the database.
    We will not be uploading using _phpMyAdmin_ because the file size is greater than the size limit provided by it.
    Open the docker container of MariaDB by running the following command:-
    ```
    docker exec -it cdlidev_mariadb_1 sh
    ```

16. Now in order to upload the database, we have to create an empty database in MySQL. To do that,Open MySQL. For this, you won't be needing any username or password since it is set to default.
	    ```
        mysql
        ```
        This will lead us to the MySQL shell.
        
    16.1 Create a database in MySQL, by running the following command:-
        
    ```
    MariaDB [(none)]> create database cdli_db;
    ```
    
	 16.2 Leave the MySQL, by running
    ```
    MariaDB [(none)]> exit
    ```

    Here `MariaDB [(none)]>` is a prompt from our MySQL shell.


17. Now you would be back on your terminal of the MariaDB docker container. Now upload the `/tmp/cdli_db.sql` of the container to the MySQL database.
    For that run the following command:
    ```
    mysql cdli_db < /tmp/cdli_db.sql
    ```
    Since it is a large database it will take some time to upload. On one instance, it took 30 minutes.

18. You can have a look at the credentials of the database [here](https://gitlab.com/cdli/framework/blob/phoenix/develop/app/cake/config/app.php#L251).

19. Now your website would be up and available at http://127.0.0.1:2354

20. Registration

    20.1 Access http://localhost:2354/register

    20.2 Input *username*, *password* and *email*.

    20.3 Click **Sign Up**.

    20.4 Use **Google Authenticator** to scan the QR code, input the *code*, back up the code and check the checkbox.

    20.5 Click **Enable 2FA**.
  
21. Log in

    21.1 Access any webpage.

    21.2 Input *username*, *password*.

    21.3 Click **Login**.

    21.4 After redicted to the 2FA page, input the *code*.

    21.5 Click **Login**.

22. In order to search:-

    22.1 Open the http://127.0.0.1:2354/search/ page

    22.2 Just in order to check as an example, in the dropdown, select `CDLI No.` option off and enter the number `111946` in the search bar. You'll see some results.<br>



# Installation on Windows

The installation process has been tested on Windows 10. For installation on Windows 10 Home Edition, please check below.

0. Prerequisites: **Git for Windows**, **Python for Windows**. Open PowerShell or Command Prompt and check if Python and Git are installed by running the following commands. If the commands are not found, install [Git](https://git-scm.com/downloads) or [Python](https://www.python.org/downloads/windows/) from their official websites.

    ```
    git --version

    python --version
    ```

1. Download **Docker Desktop for Windows** Installer from the official website. A free account on Docker would be required before the download. Following errors might occur when Docker tries to start after the installation is complete:

    1.1  **Error: Hyper-V is not enabled**:
    
    Docker runs virtualized environments right from hardware and thus requires Hyper-V. Press <kbd>Win+R</kbd> and launch `msinfo32`. Enter "BIOS" in the *Find* text box to see if your PC has UEFI or BIOS. Steps might differ slightly on different machines.

    1.1.1 PC with UEFI:

    1. Press Restart while holding Shift down.
            
    2. Goto TroubleShoot > Advanced Options > UEFI Firmware Settings > Restart.
            
    3. Press <kbd>F10</kbd> > Goto System Configuration Tab using Arrow keys > Enable Virtualization Technology. Press <kbd>F10</kbd> to save and Exit.


    1.2 PC with Legacy BIOS:

    1.2.1 Restart PC but press <kbd>Esc</kbd> as soon as anything appears on the screen. BIOS settings screen should be visible.
            
    1.2.2 Press <kbd>F10</kbd> > Goto System Configuration Tab using Arrow keys > Enable Virtualization Technology. Press <kbd>F10</kbd> to save and Exit.
    

    1.3  **Error: Docker does not have enough memory**: 
    
    Right-click on the Docker icon in the notification area. Select Settings and goto the Advanced tab to specify the memory Docker should use. Docker runs smoothly for our purposes even with minimum memory.

2. Clone the framework repository and enter GitLab credentials when asked. Use Windows PowerShell or Command Prompt.

    ```
    git clone https://gitlab.com/cdli/framework.git
    ```

3. Mounting drive 

    The drive in which the framework is located needs to be shared with Docker containers. If, for example, the framework is cloned into a folder in `C:`, then 
    
    1. Right click on the **Docker** icon and go to **Settings**
    
    2. Click **Shared Drives** tab
    
    3. Check `C:`
    
    4. Apply. 
    
    The following error might occur:

    -  **Error: Firewall blocked file sharing between Windows and containers**: 
    
        Open your Firewall program and add port 445 (Microsoft Directory Server Port) to the list of ports allowed to system services.

4. Get `docker-compose` library for Python using the following command, similar to the Step 4 in Linux installation.

    ```
    pip install docker-compose
    ```

5. Login to docker image of `orchestrator/manifests`, similar to **Step 5** in Linux installation.

    ```
    docker login https://registry.gitlab.com/
    ```

6. Start the servers in Docker containers by either using the `docker compose` command or by running the script available in `dev` folder to do the same.

    ```
    python .\dev\cdlidev up
    ```

7. Continue from **Step 7** of the Linux installation while executing the instructions on a new window of PowerShell or Command Prompt.

# Installation on Windows 10 Home Edition

0. **System requirements**

    - OS Windows 10 Home Edition, version 2004 or higher, **OS build 19041 or higher**.

        1. Go to: Windows logo key > Settings > About > Windows specifications > OS build.

        2. If the OS build in your machine is less than the required version, please upgrade using [Windows Update Assistant](https://www.microsoft.com/en-us/software-download/windows10).
       
1. **Enable the WSL 2 feature on Windows**
    
    1. Install WSL:
    
        Open PowerShell as Administrator and run:

        ```
        dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart        
        ```

        To install WSL 1, restart your machine.

    2. Upgrade to WSL 2: 
        
        Before upgrading to WSL 2, you must enable the Virtual Machine Platform optional feature. Open PowerShell as Administrator and run:

        ```
        dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
        ```

        To install and update to WSL 2, restart again your machine.

    3. Set WSL 2 as default version: 

        Run the following command in PowerShell to set WSL 2 as the default version when installing a new Linux distribution:
    
        ```
        wsl --set-default-version 2        
        ```

        After running the command, you might see this message:

        ```
        WSL 2 requires an update to its kernel component. For information please visit https://aka.ms/wsl2kernel       
        ```

        Follow the link and install the MSI to install a Linux kernel on your machine for WSL 2 to use.

    4. Install the Linux distribution: 
    
        Choose a Linux distribution from Microsoft Store and select _Get_. This installation is tested with **Ubuntu 20.04 LTS** distribution.

    5. Set up the new distribution: 
    
        Double click on the distribution when the installation is complete. A console window will open. After file de-compression, you will be asked to create a user account and password for your new Linux distribution.

    6. Set your distribution version to WSL 2: 
    
        Check the WSL version assigned to your Linux distributions. Open PowerShell and enter the command:

        ```
        wsl --list --verbose
        ```

        To set WSL 2 as your default architecture, write this command:
        
        ```
        wsl --set-default-version 2
        ```

2. **Download Docker Desktop**

    1. Download [Docker Desktop Stable 2.3.0.3](https://hub.docker.com/editions/community/docker-ce-desktop-windows/) or later release.

    2. Follow the installation instructions.

    3. Start Docker Desktop. Go to: Settings > General. Select the _Use WSL 2_ based engine checkbox (this option should be already enabled by default, since your system now supports WSL 2).

    4. Click _Apply & Restart_.

    5. When Docker restarts, go to: Settings > Resources > WSL integration. WSL integration will be enabled on your default WSL distribution. To change your default WSL distribution, run the following command:
    
        ```
        wsl --set-default <distro name>
        ```

    6. Optionally, select any additional distributions you would like to enable WSL 2 on.

    7. Click _Apply & Restart_.

3. **Install the framework**

    - Continue with the manual installation steps on Linux from 0 up to 18.
  
## Cake and Composer

Currently cake and composer would be accessible on a separate container. This container is named as `dev_cake_composer`. To do any cake or composer operations follow the steps below:

### Executing single command :
* Execute  
    
    `docker exec -t dev_cake_composer [command]`
 
###  Executing multiple commands:
* Execute  
    
    `docker exec -it dev_cake_composer sh`
    
    this command would open a sh shell and you can run your cake commands and composer commands, directly from there.

### Note :
The development container runs when you do `./dev/cdlidev.py up` and when you close it, it removes container and data. Although anything you do with `cake` & `composer` would be saved.
This container should be removed in production environment.

## Google Analytics

The framework integrates google analytics tags in headers of template files, you need to proivde with a Tracking ID to link it to your analytic account.

1. You can follow [this guide](https://support.google.com/analytics/answer/1008015?hl=en) to setup your analytics account.

2. Copy your Tracking ID (usually starting from UA).

3. Paste the Tracking ID in under `app/cake/config/.env` after the GA_CODE, it should look like `GA_CODE="UA-xxxxxxxxx-y"`.

#### Security Note:

Google analytics Tracking ID is public and hence can be misused, to prevent this misuse you can set the domains which can register analytics to a particular Tracking ID. To achieve this :

1. Log in to your analytic account.

2. Click on the `Admin` button, in the bottom of the page.

3. Now under view select `Filters`

4. Click on `Add Filter`.

5. Give a name you want to give to your filter, and select filter type to include only.

6. Select your identifier as per your choice in `Select source or destination` it can be ip address or host name for your framework deployment.

7. Select appropriate expression to match the filter, and click on save.

8. Now as per your filter only a particular host can mark analytics on the portal.

There are a lot of filter to maintain your mirror sites, campaigns, hosts and present analytics accordingly you can configure them over analytics platform only!

## Environment Variables

The environment variables are defined inside `app/cake/config/.env`.

We currently have one environment variable `APP_ENV` and this value helps us determine the environment where the application is running.

If you are running the app on a production server, update the APP_ENV variable file to `APP_ENV=production`.

If you are running the app on a development or local server, update the APP_ENV variable file to `APP_ENV=development`.

## Potentials Errors

The following errors might show up while starting the server using `docker-compose` or the provided Python script.

1.  **Cannot start service mariadb:** 
    
    Driver failed programming external connectivity on endpoint cdlidev_appname_1

    **Solution :** Restart docker

2.  **Cannot create container for service app_name:** 

    Mount denied: The source path pathTo/framework/app/upload doesn't exist and is not known to Docker.

    **Solution :** Make an empty directory named `upload` inside `app` directory.

The following errors might show up at the webapp:

1.  **Error: Could not load configuration file: /srv/app/cake/config/app.php**:

    Copy the file `app.default.php` to `app.php`.

    ```
    cp .\app\cake\config\app.default.php .\app\cake\config\app.php
    ```

2.  **Error: SQLSTATE[HY000]  [2002] No such file or directory**:

    Open `app.php` and in the group of settings for `'Datasources'`, ensure that the following settings have the correct value:

    ```
    `host' => 'mariadb',
    'port' => '3306',

    // Use as logged-in during the Step 10 of Linux installation
    'username' => 'root',
    'password' => '',

    // Use as named in Step 14 of Linux installation
    'database' => 'cdli_db',
    ```

3.  **Error: Permission denied**
    While trying to activate the activate virtual environment using the code below

    ```
    source /home/$Your_pc_username/CDLI/cdli_env/activate

                        OR

    source cdli_env/bin/activate
    ```

    ***Possible Problems***
    
    `sudo` is not used before running the script. 

    **Solution:**
    Use `sudo` as a preceeding code before running the script as in the example below:

    ```
    sudo source cdli_env/bin/activate
    ```

4. **Error sudo: source: command not found**:<br><br>
    In step **1.4** when trying to activate the virtual enviromnment before ***step2*** using the code below

    ``` 
    sudo source /home/$Your_pc_username/CDLI/cdli_env/activate

                        OR

    sudo source cdli_env/bin/activate
    ```

    ***Possible problems:***<br><br>
    It could be your **cdli_env** folder has not been created in the CDLI folder.
  
    **Solution:**
    1. Restart setup for **step 1** again using `./setup.sh` and ensure that the cdli_env folder is created inside the CDLI folder


    2. You could remove the previous folder of the **cdli_env** and rerun stage1 setup.


    3. You could also run the command `source cdli_env/bin/activate` without using `sudo`


5. **ERROR: Downloading database failed. Try again !!**


   **Possible Problems**


   1. It could be you do not have developer access to the database, contact mentors for that.


   2. It could also occur in case of network failure, in that case, try again.


   3. This error could also occur if you have not created an access token. In that case, go to gitlab and create an access token

**Solution:**


An alternative to this problem is downloading the offline database **cdli_db.gz** file, and pass the path when prompted(Non gitlab access token method).


6. **ERROR: 'cdlidev.py' status : not running !!**


   **Possible Problems:**


   This error occurs when the framework is not running and setup for **stage 3** is being initiated


   **Solution:**
   You can start the framework by running the code below
   ```
    source cdli_env/bin/activate

    ./framework/dev/cdlidev.py up
   ```

   **Note:** Be sure to run the code above in a new terminal, then restart stage 3 setup.

7. **ERROR: Got permission denied while trying to connect to the docker daemon**


    **Possible Problems:**


    1. Not running the commands in the directory, file which was originally downloaded


    2. Username is not in the docker group


    **Solution:**

    
    To avoid needing sudo for docker commands, add your username to the docker group using the code below:

    ```
    sudo usermod -aG docker $USER
    ```










