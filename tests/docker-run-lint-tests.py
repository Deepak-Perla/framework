import os
import sys
#
# This files runs the lint test for PHP, SCSS & CSS inside the docker
# container dev_cake_composer for the windows user, if you are not a windows
# user please use ./cdlitest at root.
# TODO: @NishealJ change dev_cake_composer to cdli testing container, as dev_cake_composer
# only supports composer installation and bin/bash.
#

def run_csslint_tests():
    # This function runs the commands to start lint tests for CSS inside the docker.
    command_drun_csslint = os.popen('docker exec -i dev_cake_composer sh -c "cd srv; python3 tests/run-css-lint-tests.py"')
    command_drun_csslint_result = command_drun_csslint.read().split('\n')
    print(command_drun_csslint_result)
    command_drun_csslint.close()
    
def run_scsslint_tests():
    # SCSS Linting.
    command_drun_scss = os.popen('docker exec -i dev_cake_composer sh -c "cd srv; python3 tests/run-scss-lint-tests.py"')
    command_drun_scss_result = command_drun_scss.read().split('\n')
    print(command_drun_scss_result)
    command_drun_scss.close()
    
def run_phplint_tests():
    # PHP Linting.
    command_drun_phplint = os.popen('docker exec -i dev_cake_composer sh -c "cd srv; python3 tests/run-php-lint-tests.py"')
    command_drun_phplint_result = command_drun_phplint.read().split('\n')
    print(command_drun_phplint_result)
    command_drun_phplint.close()

# Fuction call.
def main(argv):
    args = sys.argv
    for opt in args:
        if opt == '-c':
            run_csslint_tests()
            run_scsslint_tests()
            run_phplint_tests()
            sys.exit()
        if opt == '--csslint':
            run_csslint_tests()
            sys.exit()
        if opt == '--scsslint':
            run_scsslint_tests()
            sys.exit()
        if opt == '--phplint':
            run_phplint_tests()  
            sys.exit()  
        if opt == '-h':
            print ('Ussage: python tests/docker-run-lint-tests.py \n-c : For running combined tests for linting CSS, SCSS & PHP files.\n--csslint : For linting only css files.',
                   '\n--scsslint : For linting SCSS Files.\n--phplint : For linting PHP Files.')
            sys.exit()
    print ('Invaild: See -h for details.')        

if __name__ == "__main__":
   main(sys.argv[1:])