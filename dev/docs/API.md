# API development guide

This is a guide for developing and maintaining APIs in the framework.

## Entity APIs

This section concerns GET endpoints.

### Request type recognition

The request type can be determined using [`RequestHandler::accepts()`](https://api.cakephp.org/3.6/class-Cake.Controller.Component.RequestHandlerComponent.html#_accepts).
For example, to test whether the client requests a JSON file one can use

```php
$this->RequestHandler->accepts('json')
```

To implement content negotiation, it may be better to use [`RequestHandler::prefers()`](https://api.cakephp.org/3.6/class-Cake.Controller.Component.RequestHandlerComponent.html#_prefers)
instead. This prioritizes the users request over the order of implementations.

```php
$preferredType = $this->RequestHandler->prefers()
if ($preferredType === 'json') {
    // ...
}
```

See the section on content types for supported values.

### Response serialization

A number of formats can be serialized out of the box. CakePHP by default has
data views for JSON and XML, and the CsvView plugin adds support for a CSV data
view, currently also used for XLSX and TSV. To serialize custom formats add a
view class to the `viewClassMap` option of `RequestHandler` in the `initialize()`
function of `AppController.php`. These custom `View` classes can extend other
classes, for example to pre-process the data before serialization.

#### APIs needing external scripts

To set up external scripts, or specifically scripts in containers other than the
one CakePHP is in,

  1. Set up a server in the script container. If you are making a new container,
     make sure it has the `nodelocal-private` network.
  2. This API can then be called only from other containers within that network,
     as http://container-name:PORT. This currently includes most containers, but
     that may change in the future. If you want to test the API outside of the
     containers, map the ports as `internal:external` (e.g. `80:3080`) to access
     it at `http://localhost:3080` (on Linux). **This should only be for testing.**
  3. To call the API from CakePHP, use [`HttpClient`](https://api.cakephp.org/3.6/class-Cake.Http.Client.html).

Point 3 is simplified by the `ScriptsHelper`. You can load this into views like so:

```php
public function initialize()
{
    parent::initialize();
    $this->loadHelper('Scripts');
}
```

You can either call `ScriptsHelper::post()` directly, or create your own method
to simplify the developer experience a bit. For example, for references this
method exists:

```php
public function formatReference($data, $format, $options)
{
   $url = sprintf('http://node-tools:3000/format/' . $format . '?' . http_build_query($options));
   return $this->post($url, $data);
}
```

It takes the data (artifact, publication or article), the format (BibTeX, RIS,
bibliography) and other options (languages and styles for bibliographies).

`ScriptsHelper::post()` takes three parameters:

  - `$url`,
  - `$data`, the JSON payload, and
  - an optional `$key` for writing the result to cache

### Response type specification

You can specify the response type with [`RequestHandler::respondAs()`](https://api.cakephp.org/3.6/class-Cake.Controller.Component.RequestHandlerComponent.html#_respondAs)
like the code block below. This is usually called in the data view, but in the case
of CsvView the response type is overridden after the `render()` call in case
not CSV, but TSV or XLSX is requested.

```php
$this->render();
$this->RequestHandler->respondAs('json');
```

See the section on content types for supported values.

### Content types

The map of content types is relatively complete and will
not need extension for most common types. You can check whether a type is supported
in [`Response::_mimeTypes` (protected)](https://api.cakephp.org/3.6/class-Cake.Http.Response.html#$_mimeTypes). To add a custom type anyway, use
[`Response::setTypeMap()`](https://api.cakephp.org/3.6/class-Cake.Http.Response.html#_setTypeMap) in the `initialize()` function of `AppController`, generally.

```php
EventManager::instance()->on('Controller.initialize', function (Event $event) {
    $controller = $event->getSubject();
    $controller->response->setTypeMap('json', 'application/json');
});
```

Alternatively, you can create a [`Component`](https://api.cakephp.org/3.6/class-Cake.Controller.Component.html)
that sets the response types and the view class map. You can then load the
component into every controller that should support that set of formats. Keep
in mind to set the `_serialize` view variable, if your view needs that.
