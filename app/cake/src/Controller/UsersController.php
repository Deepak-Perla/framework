<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * beforeFilter method
     *
     * To set up access before this contoller is executed.
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        $requestedAction = $this->getRequest()->params['action'];

        $actionsPresent = ['login', 'register', 'profile', 'profileEdit'];

        // Check if any undefined actions are requested
        if (!in_array($requestedAction, $actionsPresent)) {
            return $this->redirect('/login');
        }

        // Action accessible only when not logged in.
        $actionForNotLoggedIn = ['login', 'register'];

        // Action accessible only when logged in.
        $actionForLoggedIn = ['profile', 'profileEdit'];

        // If Not logged in redirect to profile page
        if (is_null($this->Auth->user())) {
            $this->Auth->allow($actionForNotLoggedIn);
        } else {
            if (in_array($requestedAction, $actionForNotLoggedIn)) {
                return $this->redirect([
                    'action' => 'profile'
                ]);
            }
        }
    }

    /**
     * login method
     *
     * @return \Cake\Http\Response|void
    **/
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            // Check if login available
            if ($user) {
                // Check if user deactivated or banned
                if (!$user['active']) {
                    // If last login more than 6 months else banned
                    if (Time::now()->toUnixString() - $user['last_login_at']->toUnixString() > 6*30*24*60*60) {
                        return $this->Flash->error(__('Your account is inactive. Contact an administrator to reactivate it.'));
                    } else {
                        return $this->Flash->error(__('Your account has been banned.'));
                    }
                }

                $session = $this->getRequest()->getSession();
                $modified_user_data['username'] = $user['username'];
                $modified_user_data['2fa_status'] = $user['2fa_status'];

                // Storing in session variable 'user' : [username, 2fa_key, 2fa_status, created]
                $session->write([
                    'user' => [
                        'type' => 'login',
                        'user' => $modified_user_data,
                        'session_verified' => 0,
                        '2fa_session_created' => Time::now()
                    ]
                ]);

                return $this->redirect([
                    'controller' => 'Twofactor',
                    'action' => 'index',
                ]);
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }
    }

    /**
     * Register method
     *
     * @return \Cake\Http\Response|void
     */
    public function register()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $currentTime = Time::now();
            $data = array_merge($data, [
                'last_login_at' => $currentTime,
                'created_at' => $currentTime,
                'modified_at' => $currentTime,
                'active' => 0
            ]);

            $newUser =  $this->validateUser($data);
            $errors = $newUser->errors();
            $badpasswordStatus = $this->checkBadPasswords($newUser, $data["password"]);

            // Display errors generated after validation of user's data.
            if (!empty($errors)) {
                $this->set("errors", $errors);
            } elseif ($badpasswordStatus) {
                $this->set("badPassword", $badpasswordStatus);
            } else {
                $session = $this->getRequest()->getSession();

                // Storing in session variable 'user'
                $session->write([
                    'user' => [
                        'type' => 'register',
                        'user' => $newUser,
                        '2fa_session_created' => Time::now()
                    ]
                ]);

                // This session_verified = 'false' will be set for every new session. Will be set 'true' once user submit 2FA secret code or setup 2FA for first time.
                $this->getRequest()->getSession()->write('session_verified', 0);

                return $this->redirect([
                    'controller' => 'Twofactor',
                    'action' => 'index',
                ]);
            }
        }
    }

    /**
     * Profile method
     *
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profile($param = null)
    {
        if ($param === 'edit') {
            $this->setAction('profileEdit');
        } else {
            $username = $this->Auth->user('username');
            $user = $this->Users->findByUsername($username)->first();

            if ($user) {
                $modifiedUser = $this->Auth->user();
                $modifiedUser['username'] = $user['username'];
                $modifiedUser['email'] = $user['email'];
                $modifiedUser['last_login_at'] = $user['last_login_at'];
                $modifiedUser['active'] = $user['active'];
                $modifiedUser['modified_at'] = $user['modified_at'];
                $modifiedUser['created_at'] = $user['created_at'];
                $modifiedUser['roles'] = $this->GeneralFunctions->getUsersRole($user['id']);

                $this->set('user', $modifiedUser);
            } else {
                $this->Flash->error("No such user exists");
                return $this->redirect('/');
            }
        }
    }

    /**
     * Profile Edit method
     *
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profileEdit()
    {
        if ($this->request->is(['post'])) {
            $username = $this->Auth->user('username');
            $user = $this->Users->findByUsername($username)->first();

            $formData = $this->request->getData();

            $checkOldPassword = (new DefaultPasswordHasher())->check($formData['old_password'], $user['password']);
            if (!($checkOldPassword)) {
                $this->Flash->error(__("The old password is incorrect. Please, enter correct old password."));
            } else {
                $sameAsPreviousPassword = (new DefaultPasswordHasher())->check($formData['password'], $user['password']);

                // Password check is kept with only previous password. (Not the list of previously used passwords by that user)
                if ($sameAsPreviousPassword) {
                    $this->Flash->error(__("The new password cannot be the same as your previously used passwords."));
                } else {
                    $updateduser = $this->Users->patchEntity($user, $formData);

                    $errors = $updateduser->errors();

                    $badpasswordStatus = (new \App\Controller\UsersController())->checkBadPasswords($user, $formData['password']);

                    // Display errors generated after validation of user's data.
                    if (!empty($errors)) {
                        foreach ($errors as $error) {
                            foreach ($error as $key => $value) {
                                $this->Flash->error($value);
                            }
                        }
                    } elseif ($badpasswordStatus) {
                        $this->Flash->error("Please use a more secure password. Try with a sentence.");
                    } else {
                        if ($this->Users->save($updateduser)) {
                            $this->Flash->success(__('Your password has been updated.'));
                            return $this->redirect([
                         'action' => 'profile'
                         ]);
                        } else {
                            $this->Flash->error(__('The password could not be updated. Please, try again.'));
                        }
                    }
                }
            }
        }

        $this->set('user', $this->Auth->user());
    }


    // Validates User data using Models validation Rules
    public function validateUser($userData)
    {
        $this->loadModel('Users');
        $newUser = $this->Users->newEntity($userData);
        return $newUser;
    }

    // Check input passwords with list of bad passwords.
    public function checkBadPasswords($user, $password)
    {
        $file = "badpassword.txt";
        $file = file_get_contents($file);
        $file = explode("\n", $file);
        $email = explode('@', $user['email'])[0];
        $username = $user['username'];

        if ($password === $email || $password === $username) {
            return 1;
        }
        return in_array($password, $file);
    }
}
