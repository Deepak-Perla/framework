<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UpdateEvents Model
 *
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsToMany $Authors
 *
 * @method \App\Model\Entity\UpdateEvent get($primaryKey, $options = [])
 * @method \App\Model\Entity\UpdateEvent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UpdateEvent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UpdateEvent|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UpdateEvent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UpdateEventsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('update_events');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Authors', [
            'foreignKey' => 'update_event_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'authors_update_events'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->nonNegativeInteger('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->scalar('project')
            ->requirePresence('project', 'create')
            ->notEmpty('project');

        $validator
            ->scalar('inscription_type')
            ->requirePresence('inscription_type', 'create')
            ->notEmpty('inscription_type');

        $validator
            ->scalar('event_comments')
            ->requirePresence('event_comments', 'create')
            ->notEmpty('event_comments');

        return $validator;
    }
}
