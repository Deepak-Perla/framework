<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Periods Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\HasMany $Artifacts
 * @property \App\Model\Table\RulersTable|\Cake\ORM\Association\HasMany $Rulers
 * @property \App\Model\Table\SignReadingsTable|\Cake\ORM\Association\HasMany $SignReadings
 * @property \App\Model\Table\YearsTable|\Cake\ORM\Association\HasMany $Years
 *
 * @method \App\Model\Entity\Period get($primaryKey, $options = [])
 * @method \App\Model\Entity\Period newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Period[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Period|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Period|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Period patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Period[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Period findOrCreate($search, callable $callback = null, $options = [])
 */
class PeriodsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('periods');
        $this->setDisplayField('period');
        $this->setPrimaryKey('id');

        $this->hasMany('Artifacts', [
            'foreignKey' => 'period_id'
        ]);
        $this->hasMany('Rulers', [
            'foreignKey' => 'period_id'
        ]);
        $this->hasMany('SignReadings', [
            'foreignKey' => 'period_id'
        ]);
        $this->hasMany('Years', [
            'foreignKey' => 'period_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('sequence')
            ->allowEmpty('sequence');

        $validator
            ->scalar('period')
            ->maxLength('period', 100)
            ->requirePresence('period', 'create')
            ->notEmpty('period');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
