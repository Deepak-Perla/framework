<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsLanguage Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property int $language_id
 * @property bool|null $is_language_uncertain
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Language $language
 */
class ArtifactsLanguage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'language_id' => true,
        'is_language_uncertain' => true,
        'artifact' => true,
        'language' => true
    ];
}
