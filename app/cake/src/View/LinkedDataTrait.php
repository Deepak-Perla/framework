<?php
namespace App\View;

use App\Model\Entity\Inscription;
use Cake\ORM\Entity;
use EasyRdf\Graph;
use EasyRdf\RdfNamespace;

trait LinkedDataTrait
{
    use SerializeTrait;

    protected static $_base = 'https://cdli.ucla.edu/';

    protected static $_context = [
        'cdli' => 'https://cdli.ucla.edu/docs/vocab/0.1#',
        'crm' => 'http://www.cidoc-crm.org/cidoc-crm/',
        'crma' => 'http://www.ics.forth.gr/isl/CRMarchaeo/',
        'bibtex' => 'http://purl.org/net/nknouf/ns/bibtex#',
        'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
        'geo' => 'http://www.w3.org/2003/01/geo/wgs84_pos#',
        'osgeo' => 'http://data.ordnancesurvey.co.uk/ontology/geometry/',
        'dcmitype' => 'http://purl.org/dc/dcmitype/',
        'dcterms' => 'http://purl.org/dc/terms/',
        'nif' => 'http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#',
        'conll' => 'http://ufal.mff.cuni.cz/conll2009-st/task-description.html#',
    ];

    protected function prepareDataExport(array $data)
    {
        foreach (self::$_context as $prefix => $uri) {
            RdfNamespace::set($prefix, $uri);
        }

        $graph = new Graph();
        $graph->parse(
            json_encode($this->prepareJsonLd($data)),
            'jsonld'
        );

        foreach ($data as $entity) {
            if ($entity instanceof Inscription) {
                $this->addConllRdf($graph, $entity);
            }
        }

        return $graph;
    }

    protected function prepareJsonLd(array $data)
    {
        return [
            '@graph' => $this->makeJsonLd($data),
            '@context' => array_merge(
                [ '@base' => self::$_base],
                self::$_context
            )
        ];
    }

    protected function makeJsonLd($data)
    {
        if ($data instanceof Entity) {
            return $data->getCidocCrm();
        }

        if (is_array($data)) {
            foreach ($data as $key => $item) {
                if (is_array($item) || is_object($item)) {
                    $data[$key] = $this->makeJsonLd($item);
                } elseif (empty($item)) {
                    unset($data[$key]);
                }
            }
        }

        return $data;
    }

    protected function addConllRdf(Graph $graph, Inscription $inscription)
    {
        $this->loadHelper('Scripts');

        $base = self::$_base . $inscription->getUri();
        $graph->parse($this->Scripts->formatConllRdf(
            $inscription,
            [
                'base' => $base . '#'
            ]
        ), 'turtle');

        foreach ($graph->allOfType('nif:Sentence') as $sentence) {
            // Only attach a sentence to the inscription it belongs to (which
            // happens to be the first time this property gets added). Not the
            // prettiest solution, but there are uglier onces like checking
            // whether the URI starts with the same path (/inscription/:id).
            if (!$sentence->hasProperty('dcterms:isPartOf')) {
                $sentence->addResource('dcterms:isPartOf', $base);
            }
        }
    }
}
