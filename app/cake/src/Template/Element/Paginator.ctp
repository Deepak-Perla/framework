<div class="paginator">
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first('<i class="fa fa-angle-double-left"></i> First', array('escape' => false))?>
        <?= $this->Paginator->prev('<i class="fa fa-angle-double-left"></i> Previous', array('disabledTitle' => false,'escape' => false))?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('Next <i class="fa fa-angle-double-right"></i>', array('disabledTitle' => false,'escape' => false)) ?>
        <?= $this->Paginator->last('Last <i class="fa fa-angle-double-right"></i>', array('escape' => false)) ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>