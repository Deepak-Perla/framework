<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription[]|\Cake\Collection\CollectionInterface $inscriptions
 */
?>

<h3 class="display-4 pt-3"><?= __('Inscriptions') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_atf2conll_diff_resolved') ?></th>
            <th scope="col"><?= $this->Paginator->sort('accepted_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('accepted') ?></th>
            <th scope="col"><?= $this->Paginator->sort('update_events_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_latest') ?></th>
            <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($inscriptions as $inscription): ?>
        <tr>
            <td><?= $this->Html->link($this->Number->format($inscription->id), ['action' => 'view', $inscription->id]) ?></td>
            <td><?= $inscription->has('artifact') ? $this->Html->link($inscription->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]) : '' ?></td>
            <td><?= h($inscription->is_atf2conll_diff_resolved) ?></td>
            <td><?= $this->Number->format($inscription->accepted_by) ?></td>
            <td><?= h($inscription->accepted) ?></td>
            <td><?= $this->Number->format($inscription->update_events_id) ?></td>
            <td><?= h($inscription->is_latest) ?></td>
            <!-- <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $inscription->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $inscription->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $inscription->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscription->id)]) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php echo $this->element('Paginator'); ?>
