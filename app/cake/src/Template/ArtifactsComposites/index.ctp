<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsComposite[]|\Cake\Collection\CollectionInterface $artifactsComposites
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Composites') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('composite') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsComposites as $artifactsComposite): ?>
        <tr>
            <td><?= h($artifactsComposite->composite_no) ?></td>
            <td><?= $artifactsComposite->has('artifact') ? $this->Html->link($artifactsComposite->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsComposite->artifact->id]) : '' ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsComposite->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsComposite->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsComposite->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsComposite->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>
