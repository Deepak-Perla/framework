<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialAspect $materialAspect
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= h($materialAspect->material_aspect) ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Material Aspect') ?></th>
                    <td><?= h($materialAspect->material_aspect) ?></td>
                </tr>
                <!-- <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($materialAspect->id) ?></td>
                </tr> -->
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Material Aspect'), ['action' => 'edit', $materialAspect->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Material Aspect'), ['action' => 'delete', $materialAspect->id], ['confirm' => __('Are you sure you want to delete # {0}?', $materialAspect->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Material Aspects'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Material Aspect'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Artifacts Materials'), ['controller' => 'ArtifactsMaterials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifacts Material'), ['controller' => 'ArtifactsMaterials', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if (empty($materialAspect->artifacts_materials)): ?>
        <div class="capital-heading"><?= __('No Related Artifacts Materials') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Artifacts Materials') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('Material Id') ?></th>
                <th scope="col"><?= __('Is Material Uncertain') ?></th>
                <th scope="col"><?= __('Material Color Id') ?></th>
                <th scope="col"><?= __('Material Aspect Id') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($materialAspect->artifacts_materials as $artifactsMaterials): ?>
                <tr>
                    <td><?= h($artifactsMaterials->id) ?></td>
                    <td><?= h($artifactsMaterials->artifact_id) ?></td>
                    <td><?= h($artifactsMaterials->material_id) ?></td>
                    <td><?= h($artifactsMaterials->is_material_uncertain) ?></td>
                    <td><?= h($artifactsMaterials->material_color_id) ?></td>
                    <td><?= h($artifactsMaterials->material_aspect_id) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'ArtifactsMaterials', 'action' => 'view', $artifactsMaterials->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'ArtifactsMaterials', 'action' => 'edit', $artifactsMaterials->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'ArtifactsMaterials', 'action' => 'delete', $artifactsMaterials->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsMaterials->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


