<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsSeal[]|\Cake\Collection\CollectionInterface $artifactsSeals
 */
?>


<h3 class="display-4 pt-3"><?= __('Artifacts Seals') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('seal_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsSeals as $artifactsSeal): ?>
        <tr>
            <td><?= h($artifactsSeal->seal_no) ?></td>
            <td><?= $artifactsSeal->has('artifact') ? $this->Html->link($artifactsSeal->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsSeal->artifact->id]) : '' ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsSeal->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsSeal->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsSeal->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsSeal->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

