<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience[]|\Cake\Collection\CollectionInterface $proveniences
 */
?>

<h3 class="display-4 pt-3"><?= __('Proveniences') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr align="left">
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('provenience') ?></th>
            <th scope="col"><?= $this->Paginator->sort('region_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('geo_coordinates') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($proveniences as $provenience): ?>
        <tr align="left">
            <!-- <td><?= $this->Number->format($provenience->id) ?></td> -->
            <td><a href="/proveniences/<?=h($provenience->id)?>"><?= h($provenience->provenience) ?></a></td>
            <td><?= $provenience->has('region') ? $this->Html->link($provenience->region->region, ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]) : '' ?></td>
            <td><?= h($provenience->geo_coordinates) ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $provenience->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $provenience->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $provenience->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $provenience->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

