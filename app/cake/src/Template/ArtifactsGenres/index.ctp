<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsGenre[]|\Cake\Collection\CollectionInterface $artifactsGenres
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Genres') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('genre_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_genre_uncertain') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsGenres as $artifactsGenre): ?>
        <tr>
            <td><?= $artifactsGenre->has('artifact') ? $this->Html->link($artifactsGenre->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsGenre->artifact->id]) : '' ?></td>
            <td><?= $artifactsGenre->has('genre') ? $this->Html->link($artifactsGenre->genre->genre, ['controller' => 'Genres', 'action' => 'view', $artifactsGenre->genre->id]) : '' ?></td>
            <td><?= h($artifactsGenre->is_genre_uncertain) ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsGenre->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsGenre->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsGenre->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsGenre->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

