<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author[]|\Cake\Collection\CollectionInterface $authors
 */
?>


<h1 class="display-3 header-txt text-left">Authors Index</h1>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['character' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>

<table class="table-bootstrap my-3 mx-0">
    <thead>
        <tr>
            <th scope="col">Author Name</th>
            <th scope="col">Institution</th>
            <th scope="col">Email</th>
            <th scope="col">ORCID ID</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authors as $author): ?>
        <tr>
            <td><?= $this->Html->link($author->author, ['prefix' => false, 'action' => 'view', $author->id]) ?></td>
            <td><?= h($author->institution) ?></td>
            <td><?= h($author->email) ?></td>
            <td><?= h($author->orcid_id) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

