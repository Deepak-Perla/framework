<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDate $artifactsDate
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Date') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsDate->has('artifact') ? $this->Html->link($artifactsDate->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsDate->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Date') ?></th>
                    <td><?= $artifactsDate->has('date') ? $this->Html->link($artifactsDate->date->id, ['controller' => 'Dates', 'action' => 'view', $artifactsDate->date->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsDate->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Artifacts Date'), ['action' => 'edit', $artifactsDate->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Artifacts Date'), ['action' => 'delete', $artifactsDate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsDate->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Artifacts Dates'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifacts Date'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Dates'), ['controller' => 'Dates', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Date'), ['controller' => 'Dates', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>



