<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AgadeMail[]|\Cake\Collection\CollectionInterface $agademails
 */
?>

<h3 class="display-4 pt-3"><?= __('Agade Mails') ?></h3>
<br>

<?= $this->Form->create('CdliTags', ['controller' => 'AgadeMails','action' => 'index']); ?>

<?= $this->Form->input('filter_tag', ['options' => $cdli_tags]); ?>
<?= $this->Form->button(__('Filter'),['class' => 'btn btn-primary']); ?>
<?= $this->Form->end(); ?>

<br><br>
<table class="table-bootstrap">
    <tr>
        <th>Received Date</th>    
        <th>Title</th>
        <th>Category</th>
        <th>CDLI Tag</th>
        
    </tr>

    <?php foreach ($agademails as $agademail) : ?>
        <tr>
            <td>
                <?= $agademail->date ?>
            </td>
            <td>
                <div align="left">
                    <h5> <?= $this->Html->link($agademail->title, ['action' => 'view', $agademail->id,$filter_tag]) ?> </h5>
                    <!-- Formatting displaying clickable links -->
                    <?php $string =  nl2br(str_replace('>',' ', str_replace('<',' ',substr($agademail->content, 0, 200))));
                        $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i'; 
                        $string = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $string);
                        $search  = array('/<p>__<\/p>/', '/([a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/');
                        $replace = array('<hr />', '<a href="mailto:$1">$1</a>');
                        $processed_string = preg_replace($search, $replace, $string);
                        echo $processed_string; ?>
                    ...
                </div>
            </td>
            <td>
                <?= $agademail->category ?>
            </td>
            <td>
                <?php foreach ($agademail['cdli_tags'] as $cdli_tag): ?>
                    <p>
                    <?= str_replace(' ','&nbsp',$cdli_tag->cdli_tag) ?>
                    </p>
                <?php endforeach; ?>    
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<?php echo $this->element('Paginator'); ?>