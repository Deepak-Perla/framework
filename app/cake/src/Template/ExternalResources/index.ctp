<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource[]|\Cake\Collection\CollectionInterface $externalResources
 */
?>


<h3 class="display-4 pt-3"><?= __('External Resources') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('external_resource') ?></th>
            <th scope="col"><?= $this->Paginator->sort('base_url') ?></th>
            <th scope="col"><?= $this->Paginator->sort('project_url') ?></th>
            <th scope="col"><?= $this->Paginator->sort('abbrev') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($externalResources as $externalResource): ?>
        <tr align="left">
            <td>
                <a href="/externalResources/<?=h($externalResource->id)?>">
                <?= h($externalResource->external_resource) ?>
                </a>
            </td>
            <td><?= h($externalResource->base_url) ?></td>
            <td><?= h($externalResource->project_url) ?></td>
            <td><?= h($externalResource->abbrev) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

