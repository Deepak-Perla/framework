<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region[]|\Cake\Collection\CollectionInterface $regions
 */
?>


<h3 class="display-4 pt-3"><?= __('Regions') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('region') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($regions as $region): ?>
        <tr align="left">
<!--             <td><?= $this->Number->format($region->id) ?></td> -->
            <td><a href="/regions/<?=h($region->id)?>"><?= h($region->region) ?></a></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $region->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $region->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $region->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $region->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

