<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsShadow[]|\Cake\Collection\CollectionInterface $artifactsShadow
 */
?>


<h3 class="display-4 pt-3"><?= __('Artifacts Shadow') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('collection_location') ?></th>
            <th scope="col"><?= $this->Paginator->sort('collection_comments') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsShadow as $artifactsShadow): ?>
        <tr>
            <td><?= $artifactsShadow->has('artifact') ? $this->Html->link($artifactsShadow->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsShadow->artifact->id]) : '' ?></td>
            <td><?= h($artifactsShadow->collection_location) ?></td>
            <td><?= h($artifactsShadow->collection_comments) ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsShadow->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsShadow->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsShadow->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsShadow->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

