
<?php if ($flag == ''): ?>
    <div class="row justify-content-md-center">

        <div class="col-lg-7 boxed">
            <?= $this->Form->create($authorsPublication) ?>
                <legend class="capital-heading"><?= __('Link Author and Publication') ?></legend>
                <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Author Name: </td>
                        <td><?php echo $this->Form->control('author_id', ['label' => '', 'type' => 'text', 'maxLength' => 300, 'list' => 'authorList', 'autocomplete' => 'off']); ?></td>
                    </tr>
                    <tr>
                        <td> Publication ID: </td>
                        <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number']); ?></td>
                    </tr>
                    <tr>
                        <td> Sequence: </td>
                        <td><?php $options = [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                        6 => 6,
                        7 => 7,
                        8 => 8,
                        9 => 9,
                        10 => 10,
                    ];
                    echo $this->Form->control('sequence', ['label' => '', 'type' => 'select', 'options' => $options]); ?></td>
                    </tr>
                    <datalist id="authorList">
                        <?php foreach ($authors_names as $name): ?>    
                            <option value="<?php echo $name ?>" /> 
                        <?php endforeach ; ?>     
                    </datalist>
                </table>
                <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>

        </div>

        <div class="col-lg boxed">
            <div class="capital-heading"><?= __('Related Actions') ?></div>
            <?= $this->Html->link(__('List All Author Publication Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List All Editor Publication Links'), ['controller' => 'EditorsPublications', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
        </div>

    </div>

<?php elseif ($flag == 'author'): ?>
    <div class="row justify-content-md-center">
        <div class="col-lg boxed">
            <legend class="capital-heading"><?= __('Publication Reference') ?></legend>
            To be added
        </div>    
    </div>
    <div class="row justify-content-md-center">

    <div class="col-lg-4 boxed">
        <?= $this->Form->create($authorsPublication, ['action' => 'add/'.'author'.'/'.$id]) ?>
            <legend class="capital-heading"><?= __('Link Author') ?></legend>
            <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Publication ID: </td>
                        <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number', 'disabled' => 'disabled', 'value' => $id]);
                            echo $this->Form->control('publication_id', ['type' => 'hidden', 'value' => $id]); ?></td>
                    </tr>
                    <tr>
                        <td> Author Name: </td>
                        <td><?php echo $this->Form->control('author_id', ['label' => '', 'type' => 'text', 'maxLength' => 300, 'list' => 'authorList', 'autocomplete' => 'off']); ?></td>
                    </tr>
                    <tr>
                        <td> Sequence: </td>
                        <td><?php $options = [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                        6 => 6,
                        7 => 7,
                        8 => 8,
                        9 => 9,
                        10 => 10,
                    ];
                    echo $this->Form->control('sequence', ['label' => '', 'type' => 'select', 'options' => $options]); ?></td>
                    </tr>
                    <datalist id="authorList">
                        <?php foreach ($authors_names as $name): ?>    
                            <option value="<?php echo $name ?>" /> 
                        <?php endforeach ; ?>     
                    </datalist>
            </table>

            <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg-4 boxed">
        <?= $this->Form->create($editorsPublication, ['action' => 'add/'.'author'.'/'.$id]); ?>
            <legend class="capital-heading"><?= __('Link Editor') ?></legend>
            <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Publication ID: </td>
                        <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number', 'disabled' => 'disabled', 'value' => $id]);
                            echo $this->Form->control('publication_id', ['type' => 'hidden', 'value' => $id]); ?></td>
                    </tr>
                    <tr>
                        <td> Editor Name: </td>
                        <td><?php echo $this->Form->control('editor_id', ['label' => '', 'type' => 'text', 'maxLength' => 300, 'list' => 'authorList', 'autocomplete' => 'off']); ?></td>
                    </tr>
                    <tr>
                        <td> Sequence: </td>
                        <td><?php $options = [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                        6 => 6,
                        7 => 7,
                        8 => 8,
                        9 => 9,
                        10 => 10,
                    ];
                    echo $this->Form->control('sequence', ['label' => '', 'type' => 'select', 'options' => $options]); ?></td>
                    </tr>
                    <datalist id="authorList">
                        <?php foreach ($authors_names as $name): ?>    
                            <option value="<?php echo $name ?>" /> 
                        <?php endforeach ; ?>     
                    </datalist>
            </table>

        <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List All Author Publications Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List All Editor Publication Links'), ['controller' => 'EditorsPublications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

    </div>


    <h3 class="display-4 pt-3"><?= __('Linked Authors') ?></h3>
    <?php $this->Paginator->options(['model' => 'AuthorsPublications']); ?>
    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('author_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authorsPublications as $authorsPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($authorsPublication->id) ?></td> -->
            <td><?= $authorsPublication->has('author') ? $this->Html->link($authorsPublication->author->author, ['controller' => 'Authors', 'action' => 'view', $authorsPublication->author->id]) : '' ?></td>
            <td><?= $authorsPublication->has('publication') ? $this->Html->link($authorsPublication->publication->id, ['controller' => 'Publications', 'action' => 'view', $authorsPublication->publication->id]) : '' ?></td>
            <td><?= h($authorsPublication->sequence) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $authorsPublication->id, $flag, $id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $authorsPublication->id, $flag, $id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $authorsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

    <?php echo $this->element('Paginator'); ?>

    <h3 class="display-4 pt-3"><?= __('Linked Editors') ?></h3>
    <?php $this->Paginator->options(['model' => 'EditorsPublications']); ?>
    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('editor_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($editorsPublications as $authorsPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($authorsPublication->id) ?></td> -->
            <td><?= $authorsPublication->has('author') ? $this->Html->link($authorsPublication->author->author, ['controller' => 'Authors', 'action' => 'view', $authorsPublication->author->id]) : '' ?></td>
            <td><?= $authorsPublication->has('publication') ? $this->Html->link($authorsPublication->publication->id, ['controller' => 'Publications', 'action' => 'view', $authorsPublication->publication->id]) : '' ?></td>
            <td><?= h($authorsPublication->sequence) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['controller' => 'EditorsPublications', 'action' => 'edit', $authorsPublication->id, $flag, $id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['controller' => 'EditorsPublications', 'action' => 'delete', $authorsPublication->id, $flag, $id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $authorsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

    <?php echo $this->element('Paginator'); ?>

<?php elseif ($flag == 'publication'): ?>
    <div class="row justify-content-md-center">

    <div class="col-lg-4 boxed">
        <?= $this->Form->create($authorsPublication, ['action' => 'add/'.'2'.'/'.$id]) ?>
            <legend class="capital-heading"><?= __('Link Publication to Author') ?></legend>
            <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Author Name: </td>
                        <td><?php echo $this->Form->control('author_id', ['label' => '', 'type' => 'text', 'disabled' => 'disabled', 'value' => $authorsPublications->first()->author->author]);
                                echo $this->Form->control('author_id', ['type' => 'hidden', 'value' => $id]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication ID: </td>
                        <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number']); ?></td>
                    </tr>
                    <tr>
                        <td> Sequence: </td>
                        <td><?php $options = [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                        6 => 6,
                        7 => 7,
                        8 => 8,
                        9 => 9,
                        10 => 10,
                    ];
                    echo $this->Form->control('sequence', ['label' => '', 'type' => 'select', 'options' => $options]); ?></td>
                    </tr>
            </table>
            
            <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg-4 boxed">
        <?= $this->Form->create($editorsPublication, ['action' => 'add/'.'2'.'/'.$id]); ?>       
            <legend class="capital-heading"><?= __('Link Publication to Editor') ?></legend>
            <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Editor Name: </td>
                        <td><?php echo $this->Form->control('editor_id', ['label' => '', 'type' => 'text', 'disabled' => 'disabled', 'value' => $authorsPublications->first()->author->author]);
                                echo $this->Form->control('editor_id', ['type' => 'hidden', 'value' => $id]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication ID: </td>
                        <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number']); ?></td>
                    </tr>
                    <tr>
                        <td> Sequence: </td>
                        <td><?php $options = [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                        6 => 6,
                        7 => 7,
                        8 => 8,
                        9 => 9,
                        10 => 10,
                    ];
                    echo $this->Form->control('sequence', ['label' => '', 'type' => 'select', 'options' => $options]); ?></td>
                    </tr>
            </table>

            <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List All Author Publication Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List All Editor Publication Links'), ['controller' => 'EditorsPublications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

    </div>


    <h3 class="display-4 pt-3"><?= __('Linked Publications as Author') ?></h3>
    <?php $this->Paginator->options(['model' => 'AuthorsPublications']); ?>
    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('author_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_reference') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authorsPublications as $authorsPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($authorsPublication->id) ?></td> -->
            <td><?= $authorsPublication->has('author') ? $this->Html->link($authorsPublication->author->author, ['controller' => 'Authors', 'action' => 'view', $authorsPublication->author->id]) : '' ?></td>
            <td><?= $authorsPublication->has('publication') ? $this->Html->link($authorsPublication->publication->id, ['controller' => 'Publications', 'action' => 'view', $authorsPublication->publication->id]) : '' ?></td>
            <td><?= h($authorsPublication->sequence) ?></td>
            <td>To be added</td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $authorsPublication->id, $flag, $id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $authorsPublication->id, $flag, $id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $authorsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

    <?php echo $this->element('Paginator'); ?>


<h3 class="display-4 pt-3"><?= __('Linked Publications as Editor') ?></h3>
    <?php $this->Paginator->options(['model' => 'EditorsPublications']); ?>
    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('editor_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_reference') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($editorsPublications as $authorsPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($authorsPublication->id) ?></td> -->
            <td><?= $authorsPublication->has('author') ? $this->Html->link($authorsPublication->author->author, ['controller' => 'Authors', 'action' => 'view', $authorsPublication->author->id]) : '' ?></td>
            <td><?= $authorsPublication->has('publication') ? $this->Html->link($authorsPublication->publication->id, ['controller' => 'Publications', 'action' => 'view', $authorsPublication->publication->id]) : '' ?></td>
            <td><?= h($authorsPublication->sequence) ?></td>
            <td>To be added</td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['controller' => 'EditorsPublications', 'action' => 'edit', $authorsPublication->id, $flag, $id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['controller' => 'EditorsPublications', 'action' => 'delete', $authorsPublication->id, $flag, $id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $authorsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

    <?php echo $this->element('Paginator'); ?>

<?php elseif ($flag == 'bulk'): ?>

    <?= $this->cell('BulkUpload::confirmation', [
                        'AuthorsPublications',
                        isset($error_list_validation) ? $error_list_validation:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

    <?= $this->cell('BulkUpload', [
                        'AuthorsPublications', 
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

<?php endif; ?>