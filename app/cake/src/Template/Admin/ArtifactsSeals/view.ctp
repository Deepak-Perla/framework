<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsSeal $artifactsSeal
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Artifacts Seal') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Seal No') ?></th>
                    <td><?= h($artifactsSeal->seal_no) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsSeal->has('artifact') ? $this->Html->link($artifactsSeal->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsSeal->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsSeal->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Artifacts Seal'), ['action' => 'edit', $artifactsSeal->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Artifacts Seal'), ['action' => 'delete', $artifactsSeal->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsSeal->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts Seals'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Seal'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



