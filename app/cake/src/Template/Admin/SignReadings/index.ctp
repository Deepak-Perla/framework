<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading[]|\Cake\Collection\CollectionInterface $signReadings
 */
?>
<div class="signReadings index content">

    <div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Sign Reading'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Sign Readings'), ['controller' => 'SignReadings', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Periods'), ['controller' => 'Periods', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Period'), ['controller' => 'Periods', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Proveniences'), ['controller' => 'Proveniences', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Provenience'), ['controller' => 'Proveniences', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

    <h3 class="display-4 pt-3"><?= __('Sign Readings') ?></h3>
    <div class="table-responsive">
        <table  cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('sign_reading') ?></th>
                    <th><?= $this->Paginator->sort('sign_name') ?></th>
                    <th><?= $this->Paginator->sort('meaning') ?></th>
                    <th><?= $this->Paginator->sort('Preferred_reading') ?></th>
                    <th><?= $this->Paginator->sort('period_id') ?></th>
                    <th><?= $this->Paginator->sort('provenience_id') ?></th>
                    <th><?= $this->Paginator->sort('language_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($signReadings as $signReading): ?>
                <tr>
                    <td><?= $this->Html->link(__($signReading->sign_reading), ['action' => 'view', $signReading->id]) ?></td>
                    <td><?= h($signReading->sign_name) ?></td>
                    <td><?= h($signReading->meaning) ?></td>
                    <td><?= h($signReading->preferred_reading) ?></td>
                    <td><?= $signReading->has('period') ? $this->Html->link($signReading->period->id, ['controller' => 'Periods', 'action' => 'view', $signReading->period->period]) : '' ?></td>
                    <td><?= $signReading->has('provenience') ? $this->Html->link($signReading->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $signReading->provenience->id]) : '' ?></td>
                    <td><?= $signReading->has('language') ? $this->Html->link($signReading->language->language, ['controller' => 'Languages', 'action' => 'view', $signReading->language->language]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $signReading->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $signReading->id], ['confirm' => __('Are you sure you want to delete # {0}?', $signReading->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>
