<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsLanguage[]|\Cake\Collection\CollectionInterface $artifactsLanguages
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Artifacts Language'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Artifacts Languages') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('language_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_language_uncertain') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsLanguages as $artifactsLanguage): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsLanguage->id) ?></td> -->
            <td><?= $artifactsLanguage->has('artifact') ? $this->Html->link($artifactsLanguage->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsLanguage->artifact->id]) : '' ?></td>
            <td><?= $artifactsLanguage->has('language') ? $this->Html->link($artifactsLanguage->language->language, ['controller' => 'Languages', 'action' => 'view', $artifactsLanguage->language->id]) : '' ?></td>
            <td><?= h($artifactsLanguage->is_language_uncertain) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsLanguage->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsLanguage->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsLanguage->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsLanguage->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

