<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Date[]|\Cake\Collection\CollectionInterface $dates
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Date'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Months'), ['controller' => 'Months', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Month'), ['controller' => 'Months', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Years'), ['controller' => 'Years', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Year'), ['controller' => 'Years', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Dynasties'), ['controller' => 'Dynasties', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Dynasty'), ['controller' => 'Dynasties', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Dates') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('day_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('year_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ruler_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('absolute_year') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dates as $date): ?>
        <tr>
            <!-- <td><?= $this->Number->format($date->id) ?></td> -->
            <td><?= h($date->day_no) ?></td>
            <td><?= $date->has('month') ? $this->Html->link($date->month->month_no, ['controller' => 'Months', 'action' => 'view', $date->month->id]) : '' ?></td>
            <td><?= h($date->is_uncertain) ?></td>
            <td><?= h($date->month_no) ?></td>
            <td><?= $date->has('year') ? $this->Html->link($date->year->year_no, ['controller' => 'Years', 'action' => 'view', $date->year->id]) : '' ?></td>
            <td><?= $date->has('dynasty') ? $this->Html->link($date->dynasty->dynasty, ['controller' => 'Dynasties', 'action' => 'view', $date->dynasty->id]) : '' ?></td>
            <td><?= $date->has('ruler') ? $this->Html->link($date->ruler->ruler, ['controller' => 'Rulers', 'action' => 'view', $date->ruler->id]) : '' ?></td>
            <td><?= h($date->absolute_year) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $date->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $date->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $date->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $date->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>


