<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author[]|\Cake\Collection\CollectionInterface $authors
 */
?>

<h1 class="display-3 header-txt text-left">Authors Index</h1>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['character' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>
<table class="table-bootstrap my-3 mx-0">
    <thead>
        <tr>
            <th scope="col">Author Name</th>
            <th scope="col">Institution</th>
            <th scope="col">Email</th>
            <th scope="col">ORCID ID</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authors as $author): ?>
        <tr>
            <td><?= $this->Html->link($author->author, ['prefix' => false, 'action' => 'view', $author->id]) ?></td>
            <td><?= h($author->institution) ?></td>
            <td><?= h($author->email) ?></td>
            <td><?= h($author->orcid_id) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $author->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $author->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $author->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

