<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsMaterial[]|\Cake\Collection\CollectionInterface $artifactsMaterials
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Artifacts Material'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Material Colors'), ['controller' => 'MaterialColors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Material Color'), ['controller' => 'MaterialColors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Material Aspects'), ['controller' => 'MaterialAspects', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Material Aspect'), ['controller' => 'MaterialAspects', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Artifacts Materials') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr><!-- 
            <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('material_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_material_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('material_color_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('material_aspect_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsMaterials as $artifactsMaterial): ?>
        <tr>
           <!--  <td><?= $this->Number->format($artifactsMaterial->id) ?></td> -->
            <td><?= $artifactsMaterial->has('artifact') ? $this->Html->link($artifactsMaterial->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsMaterial->artifact->id]) : '' ?></td>
            <td><?= $artifactsMaterial->has('material') ? $this->Html->link($artifactsMaterial->material->material, ['controller' => 'Materials', 'action' => 'view', $artifactsMaterial->material->id]) : '' ?></td>
            <td><?= h($artifactsMaterial->is_material_uncertain) ?></td>
            <td><?= $artifactsMaterial->has('material_color') ? $this->Html->link($artifactsMaterial->material_color->material_color, ['controller' => 'MaterialColors', 'action' => 'view', $artifactsMaterial->material_color->id]) : '' ?></td>
            <td><?= $artifactsMaterial->has('material_aspect') ? $this->Html->link($artifactsMaterial->material_aspect->material_aspect, ['controller' => 'MaterialAspects', 'action' => 'view', $artifactsMaterial->material_aspect->id]) : '' ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsMaterial->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsMaterial->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsMaterial->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsMaterial->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

