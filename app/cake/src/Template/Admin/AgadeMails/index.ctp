<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AgadeMail[]|\Cake\Collection\CollectionInterface $agademails
 */
?>

<h3 class="display-4 pt-3"><?= __('Agade Mails') ?></h3>

<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('Manage CDLI Tags'), ['controller' => 'CdliTags', 'action' => 'index'], ['class' => 'btn-action']) ?>
</div>



<?= $this->Form->create($agademails, ['action' => 'index']); ?>

<?= $this->Form->button(__('Save Changes'),['class' => 'btn btn-primary']); ?>

<table class="table-bootstrap">
    <tr>
        <th>Received Date</th>    
        <th>Title</th>
        <th>Category</th>
        <th>CDLI Tags</th>
        <th>View Permissions</th>
    </tr>


    <?php foreach ($agademails as $key => $agademail) : ?>
        <tr>
            <td>
                <?= $agademail->date ?>
            </td>
            <td>
                <div align="left">
                    <h5> <?= $this->Html->link($agademail->title, ['action' => 'view', $agademail->id]) ?> </h5> 
                    <!-- Formatting displaying clickable links and emails -->
                    <?php $string =  nl2br(str_replace('>',' ', str_replace('<',' ',substr($agademail->content, 0, 200))));
                        $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i'; 
                        $string = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $string);
                        $search  = array('/<p>__<\/p>/', '/([a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/');
                        $replace = array('<hr />', '<a href="mailto:$1">$1</a>');
                        $processed_string = preg_replace($search, $replace, $string);
                        echo $processed_string; ?> 
                    ...
                </div>
            </td>
            <td>
                <?= $agademail->category ?>
            </td>
            <td>
                
                <?php foreach ($cdli_tags as $key2 => $cdli_tag): ?> 
                    <p>
                    <?= $this->Form->checkbox('changes[' . $key . '][cdli_tags]['.$key2.']["id"]', 
                        ['value' => $cdli_tag->id, 
                        'checked' => in_array($cdli_tag->id,array_column($agademail['cdli_tags'],'id')) ]); ?>
                        <br><?= str_replace(' ','&nbsp',$cdli_tag->cdli_tag) ?>
                    </p>
                <?php endforeach; ?>
                
            </td>
            <td>
                <?= $this->Form->checkbox('changes[' . $key . '][is_public]', ['value' => 1, 'checked' => $agademail->is_public]); ?>
                <?= $this->Form->control('changes[' . $key . '][id]', ['type' => 'hidden', 'value' => $agademail->id]); ?>
            </td>
        </tr>
    <?php endforeach; ?>


</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->Form->button(__('Save Changes'),['class' => 'btn btn-primary']); ?>
<?= $this->Form->end(); ?>