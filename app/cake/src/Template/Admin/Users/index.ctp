<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<h3 class="display-4 pt-3"><?= __('Users') ?></h3>

<div>
    <div>
        <table class="table table-hover" cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead  align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('Username') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Created On') ?></th>
                    <th class="actions" scope="col"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                    <tr align="left">
                        <td> 
                            <?= $this->Html->link(__($user->username), '/admin/users/'.$user->username) ?>
                        </td>
                        <td align="left"><?= h($user->created_at) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Edit'), '/admin/users/edit/'.$user->username) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>

