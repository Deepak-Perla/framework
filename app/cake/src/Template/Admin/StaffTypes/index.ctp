<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StaffType[]|\Cake\Collection\CollectionInterface $staffTypes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Staff Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Staff'), ['controller' => 'Staff', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Staff'), ['controller' => 'Staff', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="staffTypes index large-9 medium-8 columns content">
    <h3><?= __('Staff Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('staff_type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($staffTypes as $staffType): ?>
            <tr>
                <td><?= $this->Number->format($staffType->id) ?></td>
                <td><?= h($staffType->staff_type) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $staffType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $staffType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $staffType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $staffType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('Paginator'); ?>
</div>
