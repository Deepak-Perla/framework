<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>
<?php if ($flag != 'bulk'): ?>
    <h1 class="display-3 header-text text-left"><?= __('Add Artifact-Publication Link') ?></h1>
<?php endif ?>

<?php if ($flag == ''): ?>
    <div class="row justify-content-md-center ads">
        <div class="col-lg boxed">
            <?= $this->Form->create($artifactsPublication) ?>
                <legend class="capital-heading"><?= __('Link Artifact and Publication') ?></legend>
                <div class="layout-grid text-left">
                    <div>
                        Artifact ID:
                        <?= $this->Form->control('artifact_id', ['label' => false, 'type' => 'text']); ?>
                        Publication Comments:
                        <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                    </div>

                    <div>
                        Publication BibTex Key:
                        <?=  $this->Form->control('publication_id', ['label' => false, 'type' => 'text', 'maxLength' => 200, 'id' => 'bibtexkeyAutocomplete', 'list' => 'bibtexkeyList', 'autocomplete' => 'off']); ?>
                        <datalist id="bibtexkeyList"></datalist>
                        Exact Reference:
                        <?= $this->Form->control('exact_reference', ['label' => false, 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?>
                        Publication Type:
                        <?php $options = [
                            'primary' => 'primary',
                            'electronic' => 'electronic',
                            'citation' => 'citation',
                            'collation' => 'collation',
                            'history' => 'history',
                            'other' => 'other'
                            ];
                        echo $this->Form->control('publication_type', ['label' => false, 'type' => 'select', 'options' => $options, 'class' => 'form-select'] );?>
                    </div>
                </div>
            <?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

<?php elseif ($flag == 'artifact'): ?>
    <?= $this->cell('PublicationView', [$id]) ?>

    <div class="row justify-content-md-center ads">
        <div class="col-lg boxed">
            <?= $this->Form->create($artifactsPublication, ['action' => 'add/'.'artifact'.'/'.$id]) ?>
                <legend class="capital-heading"><?= __('Link Artifact to this publication') ?></legend>
                <?php echo $this->Form->control('publication_id', ['type' => 'hidden', 'value' => $artifactsPublications->first()->publication->bibtexkey]); ?>
                <div class="layout-grid text-left">
                    <div>
                        Artifact ID:
                        <?= $this->Form->control('artifact_id', ['label' => false, 'type' => 'text']); ?>
                        Publication Comments:
                        <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                    </div>

                    <div>
                        Exact Reference:
                        <?= $this->Form->control('exact_reference', ['label' => false, 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?>
                        Publication Type:
                        <?php $options = [
                            'primary' => 'primary',
                            'electronic' => 'electronic',
                            'citation' => 'citation',
                            'collation' => 'collation',
                            'history' => 'history',
                            'other' => 'other'
                            ];
                        echo $this->Form->control('publication_type', ['label' => false, 'type' => 'select', 'options' => $options, 'class' => 'form-select'] );?>
                    </div>
                </div>
            <?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <h3 class="display-4 pt-3 text-left"><?= __('Linked Artifacts') ?></h3>

    <div class="table-responsive">
        <table class="table-bootstrap my-3 mx-0">
            <thead>
                <tr>
                    <th scope="col">Artifact Identifier</th>
                    <th scope="col">Artifact Designation</th>
                    <th scope="col">Exact Reference</th>
                    <th scope="col">Publication Type</th>
                    <th scope="col">Publication Comments</th>
                    <th scope="col"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($artifactsPublications as $artifactsPublication): ?>
                <tr>
                    <td><?= $artifactsPublication->has('artifact') ? $this->Html->link('P'.substr("00000{$artifactsPublication->artifact->id}", -6), "/artifacts/{$artifactsPublication->artifact->id}") : '' ?></td>
                    <td><?= h($artifactsPublication->artifact->designation) ?></td>
                    <td><?= h($artifactsPublication->exact_reference) ?></td>
                    <td><?= h($artifactsPublication->publication_type) ?></td>
                    <td><?= h($artifactsPublication->publication_comments) ?></td>
                    <td>
                        <?= $this->Html->link(
                                $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                                ['action' => 'edit', $artifactsPublication->id, 'artifact', $id],
                                ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                                $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                                ['action' => 'delete', $artifactsPublication->id, 'artifact', $id],
                                ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?php echo $this->element('Paginator'); ?>
    </div>

<?php elseif ($flag == 'publication'): ?>

    <?= $this->cell('ArtifactView', [$id]); ?>

    <div class="row justify-content-md-center ads">
    <div class="col-lg boxed">
        <?= $this->Form->create($artifactsPublication, ['action' => 'add'.'/'.$flag.'/'.$id]) ?>
            <legend class="capital-heading"><?= __('Link Publication to the Artifact P'.substr("00000{$id}", -6)) ?></legend>
            <?php echo $this->Form->control('artifact_id', ['type' => 'hidden', 'value' => $id]); ?>
            <div class="layout-grid text-left">
                <div>
                    Publication Comments:
                    <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                </div>

                <div>
                    Publication BibTex Key:
                    <?=  $this->Form->control('publication_id', ['label' => false, 'type' => 'text', 'maxLength' => 200, 'id' => 'bibtexkeyAutocomplete', 'list' => 'bibtexkeyList', 'autocomplete' => 'off']); ?>
                    <datalist id="bibtexkeyList"></datalist>
                    Exact Reference:
                    <?= $this->Form->control('exact_reference', ['label' => false, 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?>
                    Publication Type:
                    <?php $options = [
                        'primary' => 'primary',
                        'electronic' => 'electronic',
                        'citation' => 'citation',
                        'collation' => 'collation',
                        'history' => 'history',
                        'other' => 'other'
                        ];
                    echo $this->Form->control('publication_type', ['label' => false, 'type' => 'select', 'options' => $options, 'class' => 'form-select'] );?>
                </div>
            </div>
        <?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
    </div>
    </div>

    <h3 class="display-4 pt-3 text-left"><?= __('Linked Publications') ?></h3>
    <div class="table-responsive">
        <table class="table-bootstrap my-3 mx-0">
        <thead>
            <tr>
                <th scope="col">BibTex Key</th>
                <th scope="col">Publication Designation</th>
                <th scope="col">Publication Reference</th>
                <th scope="col">Exact Reference</th>
                <th scope="col">Publication Type</th>
                <th scope="col">Publication Comments</th>
                <th scope="col"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artifactsPublications as $artifactsPublication): ?>
            <tr>
                <td><?= $this->Html->link($artifactsPublication->publication->bibtexkey, "/publications/{$artifactsPublication->publication_id}") ?></td>
                <td><?= h($artifactsPublication->publication->designation) ?></td>
                <td>
                    <?= $this->Scripts->formatReference($artifactsPublication->publication, 'bibliography', [
                            'template' => 'chicago-author-date',
                            'format' => 'html'
                        ]) ?>
                </td>
                <td><?= h($artifactsPublication->exact_reference) ?></td>
                <td><?= h($artifactsPublication->publication_type) ?></td>
                <td><?= h($artifactsPublication->publication_comments) ?></td>
                <td>
                    <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['action' => 'edit', $artifactsPublication->id, $flag, $id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                    <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['action' => 'delete', $artifactsPublication->id, $flag, $id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
    </div>

    <?php echo $this->element('Paginator'); ?>

<?php elseif ($flag == 'bulk'): ?>

    <?= $this->cell('BulkUpload::confirmation', [
                        'ArtifactsPublications',
                        isset($error_list_validation) ? $error_list_validation:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

    <?= $this->cell('BulkUpload', [
                        'ArtifactsPublications', 
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

<?php endif; ?>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/autocomplete.js"></script>