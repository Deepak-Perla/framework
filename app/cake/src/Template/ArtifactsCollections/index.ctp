<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCollection[]|\Cake\Collection\CollectionInterface $artifactsCollections
 */
?>


<h3 class="display-4 pt-3"><?= __('Artifacts Collections') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('collection_id') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsCollections as $artifactsCollection): ?>
        <tr>
            <td><?= $artifactsCollection->has('artifact') ? $this->Html->link($artifactsCollection->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsCollection->artifact->id]) : '' ?></td>
            <td><?= $artifactsCollection->has('collection') ? $this->Html->link($artifactsCollection->collection->collection, ['controller' => 'Collections', 'action' => 'view', $artifactsCollection->collection->id]) : '' ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsCollection->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsCollection->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsCollection->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsCollection->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

