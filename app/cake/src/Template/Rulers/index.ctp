<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ruler[]|\Cake\Collection\CollectionInterface $rulers
 */
?>


<h3 class="display-4 pt-3"><?= __('Rulers') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ruler') ?></th>
            <th scope="col"><?= $this->Paginator->sort('period_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty_id') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rulers as $ruler): ?>
        <tr align="left">
            <!-- <td><?= $this->Number->format($ruler->id) ?></td> -->
            <td><?= $this->Number->format($ruler->sequence) ?></td>
            <td><a href="/rulers/<?=h($ruler->id)?>"><?= h($ruler->ruler) ?></a></td>
            <td><?= $ruler->has('period') ? $this->Html->link($ruler->period->period, ['controller' => 'Periods', 'action' => 'view', $ruler->period->id]) : '' ?></td>
            <td><?= $ruler->has('dynasty') ? $this->Html->link($ruler->dynasty->dynasty, ['controller' => 'Dynasties', 'action' => 'view', $ruler->dynasty->id]) : '' ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $ruler->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $ruler->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $ruler->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $ruler->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>
