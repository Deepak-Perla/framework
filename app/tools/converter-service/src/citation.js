const fs = require('fs')
const path = require('path')

const Cite = require('@citation-js/core')
require('@citation-js/plugin-bibtex')
require('@citation-js/plugin-csl')
require('@citation-js/plugin-ris')
require('./plugin-cdli')

// Disable URL requests
Cite.plugins.input.removeDataParser('@else/url', true)
Cite.plugins.input.removeDataParser('@else/url', false)

// Some BibTeX ammendements
const bibtex = Cite.plugins.config.get('@bibtex')
bibtex.constants.commands.hith = 'h\u0306'
bibtex.constants.commands.Hith = 'H\u0306'
bibtex.constants.commands.everymodeprime = '\u2032'
bibtex.constants.argumentCommands.href = (url, display) => display
bibtex.constants.formattingCommands.subscript = 'subscript'
bibtex.constants.formattingCommands.superscript = 'superscript'

const express = require('express')
const app = express()

// Option parsing
function addOption (options, [key, ...path], value) {
    if (path.length === 0) {
        options[key] = value
    } else if (key in options) {
        addOption(options[key], path, value)
    } else {
        addOption(options[key] = {}, path, value)
    }
}

function createOptions (query) {
    const options = {}
    for (const param in query) {
        const path = param.split('.')
        addOption(options, path, query[param])
    }
    return options
}

const STYLES_DIR = path.resolve(__dirname, '../styles')
const { templates: styles } = Cite.plugins.config.get('@csl')

async function prepareFormatting (format, { template: style }) {
    if ((format !== 'citation' && format !== 'bibliography') || !style) {
        return
    }

    if (styles.has(style)) {
        return
    }

    const stylePath = path.resolve(STYLES_DIR, style + '.csl')

    if (path.dirname(stylePath) !== STYLES_DIR || !fs.existsSync(stylePath)) {
        return
    }

    styles.add(style, await fs.promises.readFile(stylePath, 'utf8'))
}

// Body parsing
app.use(express.text())
app.use(express.json({ limit: '250kb' }))

app.post('/format/bibliographyandcitations', async ({ body, query }, res) => {
    if (!body || Cite.plugins.input.type(body.data) === '@invalid') {
        res.sendStatus(415) // Unsupported Media Type
    } else {
        try {
            const data = await Cite.Cite.async(body.data, {
                generateGraph: false
            })

            try {
                const { bibliographyEntries, citationEntries } = body
                const options = createOptions(query)
                await prepareFormatting('bibliography', options)
                res.send(data.format(
                    'bibliographyandcitations',
                    bibliographyEntries,
                    citationEntries,
                    options
                ))
            } catch (e) {
                res.sendStatus(500)
            }
        } catch (e) {
            res.sendStatus(415)
        }
    }
})

// Parse and format data
app.post('/format/:format', async ({ body, params: { format }, query }, res) => {
    if (!Cite.plugins.output.has(format)) {
        res.sendStatus(406) // Not Acceptable
    } else if (Cite.plugins.input.type(body) === '@invalid') {
        res.sendStatus(415) // Unsupported Media Type
    } else {
        let data
        try {
            data = await Cite.Cite.async(body, {
                generateGraph: false
            })

            try {
                const options = createOptions(query)
                await prepareFormatting(format, options)
                res.send(data.format(format, options))
            } catch (e) {
                res.sendStatus(500)
            }
        } catch (e) {
            res.sendStatus(415)
        }
    }
})

app.get('/formats/input', (_, res) => res.send(Cite.plugins.input.list()))
app.get('/formats/output', (_, res) => res.send(Cite.plugins.output.list()))

module.exports = {
    app,
    message: `Running Citation.js v${Cite.version}...`
}
